import { Component, OnInit } from '@angular/core';
import { FormsService } from '../../shared/forms.service';
import { Forms } from '../../forms'
import { Router } from '@angular/router';



@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [FormsService]
})
export class ListComponent implements OnInit {
  forms: Forms[] = [];

  constructor(
    public formsService: FormsService,
    public router: Router
  ) { }

  ngOnInit() {
    this.readForms();
  }
  readForms() {
    this.formsService.readForms().subscribe(
      data => {
        console.log(data);
        this.forms = [];
      },
      error => {
        console.log(error);
      }
    )
  }
}
