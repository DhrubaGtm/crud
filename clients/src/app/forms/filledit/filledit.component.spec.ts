import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilleditComponent } from './filledit.component';

describe('FilleditComponent', () => {
  let component: FilleditComponent;
  let fixture: ComponentFixture<FilleditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilleditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilleditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
