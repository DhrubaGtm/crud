import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilleditComponent } from './filledit/filledit.component';
import { ListComponent } from './list/list.component';
import { NavbarComponent } from './navbar/navbar.component';



@NgModule({
  declarations: [FilleditComponent, ListComponent, NavbarComponent],
  imports: [
    CommonModule
  ],
  exports:[
    FilleditComponent,
    ListComponent,
    NavbarComponent
  ]
})
export class FormsModule { }
