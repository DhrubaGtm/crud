export class Forms {
    public _id?: string;
    public firstname: string;
    public middlename: string;
    public lastname: string;
    public dob: string;
    public bloodgroup: string;
}
