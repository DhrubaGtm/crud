import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Forms } from "../forms";


@Injectable({
  providedIn: 'root'
})
export class FormsService {
  private baseUri: string = "http://localhost:3000/api";
  private headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient) { }

  fillForm(form: Forms) {
    return this.http.post(this.baseUri + '/fillforms', form, { headers: this.headers });
  }

  readForms() {
    return this.http.get(this.baseUri + '/forms', { headers: this.headers });
  }

  UpdateForm(form: Forms) {
    return this.http.post(this.baseUri + '/editforms', form, { headers: this.headers });
  }

  deleteForms(id: string) {
    return this.http.post(this.baseUri + '/deleteforms'+id, { headers: this.headers });
  }

}
