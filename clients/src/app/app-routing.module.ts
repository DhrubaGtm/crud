import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './forms/list/list.component';
import { FilleditComponent } from './forms/filledit/filledit.component';


const routes: Routes = [
  {
    path:'',
    component: ListComponent
  },
  {
    path:'filledit',
    component: FilleditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
