const mongoose = require('mongoose');
// const Schema = mongoose.schema;

const formsSchema = mongoose.Schema({
    firstname: String,
    middlename: String,
    lastname:String,
    dob:Number,
    bloodgroup:String

})
const formsModel = mongoose.model('form', formsSchema);
module.exports = formsModel;