var express = require('express');
var router = express.Router();
const forms = require('../models/forms.model')


//retrieving forms
router.get('/forms', function (req, res, next) {
    forms
        .find({

        })
        .sort({
            _id: -1
        })
        .exec(function (err, forms) {
            if (err) {
                return next(err);
            } else {
                res.json(forms).status(200);
            }
        })
});

//add forms
router.post('/fillforms', function (req, res, next) {
    const newForm = new forms({
        firstname: req.body.firstname,
        middlename: req.body.middlename,
        lastname: req.body.lastname,
        dob: req.body.dob,
        bloodgroup: req.body.bloodgroup

    })

    newForm.save(function (err, done) {
        if (err) {
            return next(err);
        } else {
            res.status(200).json(done);
        }
    })
});

// find by id
router.get('/:id', function (req, res, next) {
    forms.findById(req.params.id, function (err, done) {
        if (err) {
            return next(err);
        } else {
            res.status(200).json(done)
        }
    })
})

// delete forma
router.delete('/deleteforms/:id', function (req, res, next) {
    forms.remove({ _id: req.params.id }, function (err, done) {
        if (err) {
            return next(err);
        } else {
            res.status(200).json({
                msg: 'delete succesfully successfully'
            })
        }
    })
});

// form edit
router.put('/editforms/:id', function (req, res, next) {
    forms.findById(req.params.id, function (err, forms) {
        if (err) {
            return next(err);
        } if (forms) {
            res.status(200).json(forms)
        } else {
            next({
                msg: 'can not find forms'
            })
        }
        if (req.body.firstname)
            forms.firstname = req.body.firstname;
        if (req.body.middlename)
            forms.middlename = req.body.middlename;
        if (req.body.lastname)
            forms.lastname = req.body.lastname;
        if (req.body.dob)
            forms.dob = req.body.dob;
        if (req.body.bloodgroup)
            forms.bloodgroup = req.body.bloodgroup;


        forms.save(function (err, forms) {
            if (err) {
                return next(err);
            }
            res.json(forms).status(200);
        })
    })
});




module.exports = router;
